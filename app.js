//creates the readline instance. 
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

function startprogram(){
    console.log("Choose an option:\n1.Read Package.json\n2.Display OS info\n3.Start HTTP server");
    rl.question("Type a number: ",(answer) => {
        // Evaluate which function to call.
        switch(answer){
            case '1': 
                printPackage();
                break;
            case '2':
                displayOS();
                break;
            case '3':
                runServer();
                break;
            default:
                console.log("Not a valid option...");
                startprogram();
        }
    });
}

//prints package.json 
function printPackage(){
    const pjson = require('./package.json');
    console.log(pjson);
    cont();
}

//Prints out the relevant OS data
function displayOS(){
    const os = require('os');
    console.log("System memory: ",(os.totalmem()/1024/1024/1024).toFixed(2) + "GB");
    console.log("Available memory: ",(os.freemem()/1024/1024/1024).toFixed(2) + "GB");
    console.log("CPU Cores: ", (os.cpus().length));
    console.log("ARCH: ", (os.arch()));
    console.log("Platform: ", (os.platform()));
    console.log("Release: ", (os.release()));
    console.log("User: ", (os.userInfo().username));
    cont();
}

//Runs a server on port 3000, wich displays hello world
function runServer(){
    const http = require('http');
    const hostname = 'localhost';
    const port = 3000;

    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
    });

    server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/\nlistening on port ${port}`);
    });
    cont();
}

//continue program
function cont(){
    rl.question("Press any key to continue\n",() => {
        startprogram()
    }); 
}

startprogram();