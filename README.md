Task 2, node.js console application. 

Console application which gives the user three options:

1. print out the package.json file. 
2. Display OS info. 
3. Start a local HTTP server on port 3000, which displays 'Hello World'.

The program is console based, and can be used by giving options to the console.
